﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Api.Messages;

public class ResponseManager : MonoBehaviour, IInputInterface
{
    private bool canExecute = false;

    #region API
    public void Init()
    {
        ChallengeTurnTakenMessage.Listener += HandleTurnTakenMessage;
    }

    public void TakeTurn()
    {
        canExecute = true;
    }
    #endregion

    #region Handlers
    private void HandleTurnTakenMessage(ChallengeTurnTakenMessage _message)
    {
        if (canExecute)
        {
            canExecute = false;

            IMove _move = new SimpleMove((int)_message.ScriptData.GetInt("x"), (int)_message.ScriptData.GetInt("y"));
            MoveManager.OnBroadcastMove(_move, true);
        }
    }
    #endregion

    private void OnDisable()
    {
        ChallengeTurnTakenMessage.Listener -= HandleTurnTakenMessage;
    }
}
