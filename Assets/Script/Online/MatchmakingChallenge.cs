﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Api.Messages;
using GameSparks.Core;
using System.Linq;

public class MatchmakingChallenge : MonoBehaviour
{
    [Header("Matchmaking Settings")]
    [SerializeField]
    private float matchmakingTimeout = 300f;
    [SerializeField]
    private float challengeExpireTimeout = 120f;

    private bool matchFound = false;

    #region API
    public void Init()
    {
        Authentication();
    }
    #endregion

    private void Authentication()
    {
        new DeviceAuthenticationRequest()
            .Send((response) =>
            {
                if (!response.HasErrors) 
                {
                    FindMatch();
                    GameManager.instance.playerId = response.UserId;
                }
            });
    }

    private void FindMatch()
    {
        List<string> codes = new List<string> { "ChallengeForza" };

        new FindChallengeRequest()
            .SetAccessType("PUBLIC")
            .SetShortCode(codes)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    if (response.ChallengeInstances.Count() == 0)
                        CreateChallenge();
                    else
                        JoinChallenge(response);
                }
            });
    }

    private void JoinChallenge(FindChallengeResponse res)
    {
        new JoinChallengeRequest()
            .SetChallengeInstanceId(res.ChallengeInstances.First().ChallengeId)
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    ChallengeStartedMessage.Listener += HandleChallengeStarted;
                    Debug.Log("Join " + res.ChallengeInstances.First().ChallengeId);
                    return;
                }
                else
                    FindMatch();

            });
    }

    private void CreateChallenge()
    {
        ChallengeStartedMessage.Listener += HandleChallengeStarted;

        new CreateChallengeRequest()
            .SetAccessType("PUBLIC")
            .SetAutoStartJoinedChallengeOnMaxPlayers(true)
            .SetChallengeShortCode("ChallengeForza")
            .SetMaxPlayers(2)
            .SetMinPlayers(2)
            .SetEndTime(System.DateTime.UtcNow.AddHours(1))
            .SetExpiryTime(System.DateTime.UtcNow.AddMinutes(challengeExpireTimeout))
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    ChallengeExpiredMessage.Listener += HandleChallengeExpired;
                    Debug.Log("Join " + response.ChallengeInstanceId);
                }
            });
    }

    #region Handlers
    private void HandleChallengeStarted(ChallengeStartedMessage _message)
    {
        matchFound = true;
        ChallengeExpiredMessage.Listener -= HandleChallengeExpired;

        GameManager.instance.challengeId = _message.Challenge.ChallengeId;
        GameManager.instance.startingPlayerId = _message.Challenge.NextPlayer;

        GameManager.instance.OnStateChange(GameState.OnlinePLaySetup);
    }

    private void HandleChallengeExpired(ChallengeExpiredMessage _message)
    {
        Debug.Log("Challenge Expired");
        FindMatch();
    }


    #endregion

    #region Coroutines
    private IEnumerator MatchmakingTImeout()
    {
        yield return new WaitForSeconds(matchmakingTimeout);

        if (!matchFound)
            Debug.Log("No Match Found");
    }
    #endregion

    private void OnDestroy()
    {
        ChallengeStartedMessage.Listener -= HandleChallengeStarted;
        ChallengeExpiredMessage.Listener -= HandleChallengeExpired;
    }

}
