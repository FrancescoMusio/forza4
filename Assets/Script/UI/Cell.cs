﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Cell : MonoBehaviour
{
    #region Delegates
    public Action<int, int> OnClickEvent;
    #endregion

    public int x;
    public int y;

    public void Init(int _x, int _y)
    {
        x = _x;
        y = _y;
    }

    public void OnClick()
    {
        if (OnClickEvent != null)
            OnClickEvent(x, y);
    }
}
