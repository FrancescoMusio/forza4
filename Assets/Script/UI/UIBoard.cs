﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBoard : MonoBehaviour
{
    [Header("Colors")]
    [SerializeField]
    private Color32 playerColor;
    [SerializeField]
    private Color32 opponentColor;
    [SerializeField]
    private Color32 freeColor;

    [Header("Board Components")]
    [SerializeField]
    private GameObject rowPrefab;
    [SerializeField]
    private GameObject columnPrefab;

    [Header("Board Reference")]
    [SerializeField]
    private Transform gameArea;

    private BoardManager boardMng;
    private PlayerInput player;

    private void CreateBoard(PlayerInput _player)
    {
        float rowHeight = 1.0f / boardMng.GetRowsCount();
        float columnLenght = 1.0f / boardMng.GetColumnsCount();

        float length = gameArea.GetComponent<RectTransform>().rect.width;

        RectTransform _rect;
        
        for (int i = 0; i < boardMng.GetRowsCount(); i++)
        {
            GameObject _newRow = Instantiate(rowPrefab, gameArea);
            _newRow.name = "Row" + i;
            _rect = _newRow.GetComponent<RectTransform>();
            _rect.anchorMin = new Vector2(0, i * rowHeight);
            _rect.anchorMax = new Vector2(1, (i + 1) * rowHeight);

            for (int j = 0; j < boardMng.GetColumnsCount(); j++)
            {
                GameObject _newColumn = Instantiate(columnPrefab, _newRow.transform);
                _newColumn.name = "Column" + j;
                _rect = _newColumn.GetComponent<RectTransform>();
                _rect.anchorMin = new Vector2(j * columnLenght, 0);
                _rect.anchorMax = new Vector2((j + 1) * columnLenght, 1);
                Cell _cell = _newColumn.GetComponent<Cell>();
                _cell.Init(i, j);
                _cell.OnClickEvent += player.HandleOnClick;
            }
        }
    }

    private void UpdateColors()
    {
        for (int i = 0; i < boardMng.GetRowsCount(); i++)
        {
            for (int j = 0; j < boardMng.GetColumnsCount(); j++)
            {
                switch (boardMng.checkCell(i, j))
                {
                    case cellStatus.Free:
                        gameArea.GetChild(i).GetChild(j).GetComponent<Image>().color = freeColor;
                        break;
                    case cellStatus.Player:
                        gameArea.GetChild(i).GetChild(j).GetComponent<Image>().color = playerColor;
                        break;
                    case cellStatus.Opponent:
                        gameArea.GetChild(i).GetChild(j).GetComponent<Image>().color = opponentColor;
                        break;
                    default:
                        break;
                }
            }
        }
    }

    #region API
    public void Init(BoardManager _boardMng, PlayerInput _player)
    {
        boardMng = _boardMng;
        player = _player;

        CreateBoard(player);
        UpdateColors();
    }

    public void updateBoard()
    {
        UpdateColors();
    }
    #endregion
}
