﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDuringGame : MonoBehaviour
{
    [Header("Users Glow")]
    [SerializeField]
    private Image UserGlow;
    [SerializeField]
    private Image OppoGlow;

    private TurnManager turnMng;

    #region API
    public void Init(TurnManager _turnMng)
    {
        turnMng = _turnMng;

        turnMng.OnTurnChange += HandleChangeTurn;
    }
    #endregion

    #region Handlers
    private void HandleChangeTurn(turnStatus _new)
    {
        if (_new == turnStatus.playerTurn)
        {
            UserGlow.gameObject.SetActive(true);
            OppoGlow.gameObject.SetActive(false);
        }
        else
        {
            UserGlow.gameObject.SetActive(false);
            OppoGlow.gameObject.SetActive(true);
        }
    }
    #endregion
}
