﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCanvas : MonoBehaviour
{
    #region OnClick
    public void OnClickOffline()
    {
        GameManager.instance.OnStateChange(GameState.OfflinePlaySetup);
    }

    public void OnClickOnline()
    {
        GameManager.instance.OnStateChange(GameState.MatchMaking);
    }
    #endregion
}
