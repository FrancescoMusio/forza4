﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class OpponentManager : MonoBehaviour, IInputInterface
{
    private BoardManager boardMng;

    #region API
    public void Init(BoardManager _boardMng)
    {
        boardMng = _boardMng;
    }

    public void TakeTurn()
    {
        StartCoroutine(CWaitForMove());
    }
    #endregion

    #region Coroutines
    private IEnumerator CWaitForMove()
    {
        float _wait = UnityEngine.Random.Range(0.1f, 0.3f);

        yield return new WaitForSeconds(_wait);

        int _x = UnityEngine.Random.Range(0, boardMng.GetRowsCount());
        int _y = UnityEngine.Random.Range(0, boardMng.GetColumnsCount());

        Debug.Log("oppo: " + _x + " , " + _y);

        IMove _move = new SimpleMove(_x, _y);
        MoveManager.OnMove(_move);
    }
    #endregion
}
