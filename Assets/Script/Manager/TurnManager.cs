﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TurnManager : MonoBehaviour
{
    #region Delegates
    public Action<bool, int, int> OnAnalyzedMove;
    public Action<turnStatus> OnTurnChange;
    #endregion

    private int turnCount;
    private turnStatus currentTurnStatus;

    private BoardManager boardMng;

    private IInputInterface playerInput;
    private IInputInterface opponentInput;
    private IInputInterface currentInput;

    private void TakeTurn()
    {
        turnCount++;

        currentInput.TakeTurn();
    }

    #region API
    public void Init(turnStatus _new, BoardManager _boardMng, IInputInterface _player, IInputInterface _opponentMng)
    {
        boardMng = _boardMng;
        turnCount = 0;

        playerInput = _player;
        opponentInput = _opponentMng;

        if (_new == turnStatus.playerTurn)
            currentInput = playerInput;
        else
            currentInput = opponentInput;

        currentTurnStatus = _new;

        OnAnalyzedMove += HandleMove;
    }

    public void BeginGame()
    {
        TakeTurn();

        if (OnTurnChange != null)
            OnTurnChange(currentTurnStatus);
    }
    #endregion

    #region Handlers
    private void HandleMove(bool _valid, int _x, int _y)
    {

        if (_valid)
        {
            if (currentInput == playerInput)
                currentInput = opponentInput;
            else
                currentInput = playerInput;

            switch (currentTurnStatus)
            {
                case turnStatus.playerTurn:
                    currentTurnStatus = turnStatus.opponentTurn;
                    break;
                case turnStatus.opponentTurn:
                    currentTurnStatus = turnStatus.playerTurn;
                    break;
            }

            if (OnTurnChange != null)
                OnTurnChange(currentTurnStatus);
        }

        //if (_valid)
        //{
        //    if (currentTurnStatus == turnStatus.playerTurn)
        //    {
        //        currentTurnStatus = turnStatus.opponentTurn;
        //        boardMng.MakeMove(cellStatus.Player, _x, _y);
        //        if (boardMng.CheckVictory(cellStatus.Player, _x, _y))
        //        {
        //            Debug.Log("Player Win");
        //            return;
        //        }
        //    }
        //    else
        //    {
        //        currentTurnStatus = turnStatus.playerTurn;
        //        boardMng.MakeMove(cellStatus.Opponent, _x, _y);
        //        if (boardMng.CheckVictory(cellStatus.Opponent, _x, _y))
        //        {
        //            Debug.Log("Oppo Win");
        //            return;
        //        }
        //    }
        //}

        TakeTurn();
        return;
    }

    //private void HandlePlayerTurnTaken(int _x, int _y)
    //{
    //    if (currentTurnStatus == turnStatus.playerTurn)
    //    {
    //        if (boardMng.canOccupy(_x, _y))
    //        {
    //            boardMng.MakeMove(cellStatus.Player, _x, _y);

    //            currentTurnStatus = turnStatus.opponentTurn;
    //        }

    //        TakeTurn();
    //    }
    //}

    //private void HandleOpponentTurnTaken(int _x, int _y)
    //{
    //    if (currentTurnStatus == turnStatus.opponentTurn)
    //    {
    //        if (boardMng.canOccupy(_x, _y))
    //        {
    //            boardMng.MakeMove(cellStatus.Opponent, _x, _y);
    //            currentTurnStatus = turnStatus.playerTurn;
    //        }

    //        TakeTurn();
    //    }
    //}
    #endregion

    #region Getters
    public turnStatus GetCurrentTurnStatus()
    {
        return currentTurnStatus;
    }
    #endregion
}

public enum turnStatus
{
    playerTurn,
    opponentTurn,
}
