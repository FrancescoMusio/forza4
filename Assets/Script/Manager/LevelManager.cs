﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoardManager))]
[RequireComponent(typeof(TurnManager))]
public class LevelManager : MonoBehaviour
{
    [Header("Scene Paramenters")]
    [SerializeField]
    private bool offlineMode = true;
    [SerializeField]
    private bool playerStart = true;

    public UIDuringGame gameUiMng;

    private BoardManager boardMng;
    private TurnManager turnMng;
    private OpponentManager opponentMng;
    private ResponseManager responseMng;
    private MoveManager moveMng;

    private PlayerInput player;

    #region API
    public void Init(bool _offlineMode, bool _playerStart)
    {
        offlineMode = _offlineMode;
        playerStart = _playerStart;

        player = FindObjectOfType<PlayerInput>();

        boardMng = GetComponent<BoardManager>();
        if (boardMng != null)
            boardMng.Init(player);

        opponentMng = GetComponent<OpponentManager>();
        if (opponentMng != null)
            opponentMng.Init(boardMng);

        responseMng = GetComponent<ResponseManager>();
        if (responseMng != null)
            responseMng.Init();

        turnMng = GetComponent<TurnManager>();
        if (turnMng != null)
        {
            if (offlineMode) 
                turnMng.Init((playerStart) ? turnStatus.playerTurn : turnStatus.opponentTurn, boardMng, player, opponentMng);
            else
                turnMng.Init((playerStart) ? turnStatus.playerTurn : turnStatus.opponentTurn, boardMng, player, responseMng);
        }

        if (gameUiMng != null)
            gameUiMng.Init(turnMng);

        moveMng = GetComponent<MoveManager>();
        if (moveMng != null)
            moveMng.Init(turnMng, boardMng, !offlineMode);

        GameManager.instance.OnStateChange(GameState.Play);
    }

    public void BeginGame()
    {
        turnMng.BeginGame();
    }
    #endregion
}
