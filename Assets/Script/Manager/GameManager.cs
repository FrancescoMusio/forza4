﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class GameManager : MonoBehaviour
{
    #region Delegates
    public Action<GameState> OnStateChange;
    #endregion

    public static GameManager instance = null;

    public bool playOffline = true;
    public bool playerStart = false;
    public string playerId = "";
    public string startingPlayerId = "";
    public string challengeId = "";
    public LevelManager levelMng;

    private GameState currentGameState;

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
            currentGameState = GameState.MainMenu;
            OnStateChange += OnStateChangeHandler;
        }
        else
        {
            DestroyImmediate(this);
        }
    }


    #region StateHandler
    private void OnStateChangeHandler(GameState _new)
    {
        currentGameState = _new;

        switch (_new)
        {
            case GameState.MainMenu:
                levelMng = null;
                break;
            case GameState.OfflinePlaySetup:
                levelMng = null;
                StartCoroutine(CGoToOfflinePlaySetup());
                break;
            case GameState.MatchMaking:
                levelMng = null;
                StartCoroutine(CGoToMatchmaking());
                break;
            case GameState.OnlinePLaySetup:
                levelMng = null;
                StartCoroutine(CGoToOnlinePlaySetup());
                break;
            case GameState.Play:
                levelMng.BeginGame();
                break;
            default:
                break;
        }
    }
    #endregion

    #region Coroutines
    private IEnumerator CGoToOfflinePlaySetup()
    {
        playOffline = true;
        playerStart = true;

        AsyncOperation load = SceneManager.LoadSceneAsync("GameScene");

        while (!load.isDone)
            yield return null;

        levelMng = FindObjectOfType<LevelManager>();
        if (levelMng != null)
            levelMng.Init(playOffline, playerStart);
    }

    private IEnumerator CGoToMatchmaking()
    {
        AsyncOperation load = SceneManager.LoadSceneAsync("Matchmaking");

        while (!load.isDone)
            yield return null;

        MatchmakingChallenge matchmaking = FindObjectOfType<MatchmakingChallenge>();
        if (matchmaking != null)
            matchmaking.Init();
    }

    private IEnumerator CGoToOnlinePlaySetup()
    {
        playOffline = false;
        playerStart = playerId == startingPlayerId;

        AsyncOperation load = SceneManager.LoadSceneAsync("GameScene");

        while (!load.isDone)
            yield return null;

        levelMng = FindObjectOfType<LevelManager>();
        if (levelMng != null)
            levelMng.Init(playOffline, playerStart);
    }
    #endregion
}

public enum GameState
{
    MainMenu,
    OfflinePlaySetup,
    MatchMaking,
    OnlinePLaySetup,
    Play
}
