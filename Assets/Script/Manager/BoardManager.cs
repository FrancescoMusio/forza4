﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    [Header("Game Board Options")]
    [SerializeField]
    private int rows = 6;
    [SerializeField]
    private int columns = 7;

    [Header("Game Board UI")]
    [SerializeField]
    private UIBoard gameBoardUI;

    private cellStatus[,] gameBoard;
    private PlayerInput player;

    #region API
    public void Init(PlayerInput _player)
    { 
        gameBoard = new cellStatus[rows, columns];


        if (gameBoardUI != null)
            gameBoardUI.Init(this, _player);
    }
    #endregion

    #region Cell
    public cellStatus checkCell(int _row, int _column)
    {
        return gameBoard[_row, _column];
    }

    public bool canOccupy(int _row, int _column)
    {
        if (_row >= 0 && _row < rows && _column >= 0 && _column < columns)
        {
            if ((_row == 0 || (_row > 0 && gameBoard[_row - 1, _column] != cellStatus.Free)) && gameBoard[_row, _column] == cellStatus.Free)
                return true;
        }

        return false;
    }

    public bool CheckVictory(cellStatus _toCheck, int _x, int _y)
    {
        // VERTICAL CHECK
        if (_x + 1 < rows && gameBoard[_x + 1, _y] == _toCheck)
        {
            if (_x + 2 < rows && gameBoard[_x + 2, _y] == _toCheck)
            {
                if (_x + 3 < rows && gameBoard[_x + 3, _y] == _toCheck)
                {
                    return true;
                }
                else if (_x - 1 >= 0 && gameBoard[_x - 1, _y] == _toCheck)
                {
                    return true;
                }
            }
            else if (_x - 1 >= 0 && gameBoard[_x - 1, _y] == _toCheck)
            {
                if (_x - 2 >= 0 && gameBoard[_x - 2, _y] == _toCheck)
                {
                    return true;
                }
            }
        }
        else if (_x - 1 >= 0 && gameBoard[_x - 1, _y] == _toCheck)
        {
            if (_x - 2 >= 0 && gameBoard[_x - 2, _y] == _toCheck)
            {
                if (_x - 3 >= 0 && gameBoard[_x - 3, _y] == _toCheck)
                {
                    return true;
                }
            }
        }

        // HORIZONTAL CHECK
        if (_y + 1 < columns && gameBoard[_x, _y + 1] == _toCheck)
        {
            if (_y + 2 < columns && gameBoard[_x, _y + 2] == _toCheck)
            {
                if (_y + 3 < columns && gameBoard[_x, _y + 3] == _toCheck)
                {
                    return true;
                }
                else if (_y - 1 >= 0 && gameBoard[_x, _y - 1] == _toCheck)
                {
                    return true;
                }
            }
            else if (_y - 1 >= 0 && gameBoard[_x, _y - 1] == _toCheck)
            {
                if (_y - 2 >= 0 && gameBoard[_x, _y - 2] == _toCheck)
                {
                    return true;
                }
            }
        }
        else if (_y - 1 >= 0 && gameBoard[_x, _y - 1] == _toCheck)
        {
            if (_y - 2 >= 0 && gameBoard[_x, _y - 2] == _toCheck)
            {
                if (_y - 3 >= 0 && gameBoard[_x, _y - 3] == _toCheck)
                {
                    return true;
                }
            }
        }

        // / CHECK
        if (_x + 1 < rows && _y + 1 < columns && gameBoard[_x + 1, _y + 1] == _toCheck)
        {
            if (_x + 2 < rows && _y + 2 < columns && gameBoard[_x + 2, _y + 2] == _toCheck)
            {
                if (_x + 3 < rows && _y + 3 < columns && gameBoard[_x + 3, _y + 3] == _toCheck)
                {
                    return true;
                }
                else if (_x - 1 >= 0 && _y - 1 >= 0 && gameBoard[_x - 1, _y - 1] == _toCheck)
                {
                    return true;
                }
            }
            else if (_x - 1 >= 0 && _y - 1 >= 0 && gameBoard[_x - 1, _y - 1] == _toCheck)
            {
                if (_x - 2 >= 0 && _y - 2 >= 0 && gameBoard[_x - 2, _y - 2] == _toCheck)
                {
                    return true;
                }
            }
        }
        else if (_x - 1 >= 0 && _y - 1 >= 0 && gameBoard[_x - 1, _y - 1] == _toCheck)
        {
            if (_x - 2 >= 0 && _y - 2 >= 0 && gameBoard[_x - 2, _y - 2] == _toCheck)
            {
                if (_x - 3 >= 0 && _y - 3 >= 0 && gameBoard[_x - 3, _y - 3] == _toCheck)
                {
                    return true;
                }
            }
        }

        // \ CHECK
        if (_x + 1 < rows && _y - 1 >= 0 && gameBoard[_x + 1, _y - 1] == _toCheck)
        {
            if (_x + 2 < rows && _y - 2 >= 0 && gameBoard[_x + 2, _y - 2] == _toCheck)
            {
                if (_x + 3 < rows && _y - 3 >= 0 && gameBoard[_x + 3, _y - 3] == _toCheck)
                {
                    return true;
                }
                else if (_x - 1 >= 0 && _y + 1 < columns && gameBoard[_x - 1, _y + 1] == _toCheck)
                {
                    return true;
                }
            }
            else if (_x - 1 >= 0 && _y + 1 < columns && gameBoard[_x - 1, _y + 1] == _toCheck)
            {
                if (_x - 2 >= 0 && _y + 2 < columns && gameBoard[_x - 2, _y + 2] == _toCheck)
                {
                    return true;
                }
            }
        }
        else if (_x - 1 >= 0 && _y + 1 < columns && gameBoard[_x - 1, _y + 1] == _toCheck)
        {
            if (_x - 2 >= 0 && _y + 2 < columns && gameBoard[_x - 2, _y + 2] == _toCheck)
            {
                if (_x - 3 >= 0 && _y + 3 < columns && gameBoard[_x - 3, _y + 3] == _toCheck)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public void MakeMove(cellStatus _new, int _x, int _y)
    {
        gameBoard[_x, _y] = _new;

        gameBoardUI.updateBoard();
    }
    #endregion

    #region Getter
    public cellStatus[,] GetBoard()
    {
        return gameBoard;
    }

    public int GetRowsCount()
    {
        return rows;
    }

    public int GetColumnsCount()
    {
        return columns;
    }
    #endregion
}

public enum cellStatus
{
    Free,
    Player,
    Opponent,
}
