﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GameSparks.Api.Requests;
using GameSparks.Api.Messages;

public class MoveManager : MonoBehaviour
{
    #region Delegates
    public static Action<IMove> OnMove;
    public static Action<IMove,bool> OnBroadcastMove;
    #endregion

    private BoardManager boardMng;
    private TurnManager turnMng;

    private bool canExecute = false;
    private bool isOnline = false;

    #region API
    public void Init(TurnManager _turnMng, BoardManager _boardMng, bool _isOnline)
    {
        boardMng = _boardMng;
        turnMng = _turnMng;
        isOnline = _isOnline;

        OnMove += HandleOnMove;
        OnBroadcastMove += HandleOnBroadcastMove;
        ChallengeTurnTakenMessage.Listener += HandleTurnTakenMessage;
    }
    #endregion

    #region Handlers
    private void HandleOnMove(IMove _move)
    {
        int _x = _move.GetX();
        int _y = _move.GetY();

        bool _valid = boardMng.canOccupy(_x, _y);

        if (_valid)
        {
            if (isOnline)
            {
                new LogChallengeEventRequest()
                    .SetEventKey("takeTurn")
                    .SetChallengeInstanceId(GameManager.instance.challengeId)
                    .SetEventAttribute("x", _x)
                    .SetEventAttribute("y", _y)
                    .SetEventAttribute("turn", 0)
                    .Send((response) =>
                    {
                        if (!response.HasErrors)
                        {
                            canExecute = true;
                            Debug.Log("Challenge mandata");
                        }
                    });
            }
            else
            {
                OnBroadcastMove(_move, true);
            }

            //if (boardMng.CheckVictory(_target, _x, _y))
            //{
            //    Debug.Log(_target + " Win");
            //    return;
            //}
        }
        else
        {
            if (turnMng.OnAnalyzedMove != null)
                turnMng.OnAnalyzedMove(_valid, _x, _y);
        }
    }

    private void HandleTurnTakenMessage(ChallengeTurnTakenMessage _message)
    {
        if (canExecute)
        {
            canExecute = false;

            IMove _move = new SimpleMove((int)_message.ScriptData.GetInt("x"), (int)_message.ScriptData.GetInt("y"));
            OnBroadcastMove(_move, true);
        }
    }

    private void HandleOnBroadcastMove(IMove _move, bool _valid)
    {
        int _x = _move.GetX();
        int _y = _move.GetY();
        cellStatus _target = (turnMng.GetCurrentTurnStatus() == turnStatus.playerTurn) ? cellStatus.Player : cellStatus.Opponent;
        boardMng.MakeMove(_target, _x, _y);

        if (turnMng.OnAnalyzedMove != null)
            turnMng.OnAnalyzedMove(_valid, _x, _y);
    }
    #endregion

    private void OnDisable()
    {
        ChallengeTurnTakenMessage.Listener -= HandleTurnTakenMessage;
    }
}

public abstract class BaseMove : IMove
{
    protected int x;
    protected int y;

    public virtual int GetX()
    {
        return x;
    }

    public virtual int GetY()
    {
        return y;
    }
}

public class SimpleMove : BaseMove
{
    public SimpleMove(int _x, int _y)
    {
        x = _x;
        y = _y;
    }
}
