﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class PlayerInput : MonoBehaviour, IInputInterface
{
    private bool playerTurn = false;

    public void TakeTurn()
    {
        playerTurn = true;
    }

    public void HandleOnClick(int _x, int _y)
    {
        if (playerTurn)
        {
            playerTurn = false;
            Debug.Log(_x + " , " + _y);

            IMove _move = new SimpleMove(_x, _y);
            MoveManager.OnMove(_move);
        }
    }
}
