﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMove
{
    int GetX();
    int GetY();
}
